import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css'],
})
export class FormularioComponent {
  @Output() resultadoCalculado = new EventEmitter<string>();

  operandoA: string;
  operandoB: string;

  sumarOperandos(): void {
    let resultado = this.operandoA + this.operandoB;
    this.resultadoCalculado.emit(resultado);
  }
}
